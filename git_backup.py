#!/usr/bin/python3

import tempfile
import sh
import os
import pytest
import time
import re
import io
import urllib

#TODO add messages to reflog
# use logging instead of print
# is pytest available on intended target images?
# make better use of sh

def backup_refs(target):
  seen = 'refs/backup/last-seen-refs/'
  last_seen_refs = set(git_refs_filter(target, seen))
  print(last_seen_refs)
  g = GitRefUpdate(target)
  for ref in git_refs(target):
    object_hash, object_type, name = ref.split()
    types = ['blob', 'tree', 'commit', 'tag']
    if object_type not in types:
      raise ValueError('Git ref object type may only be one of %s, but for %s it is: %s' % (types, name, object_type))
    if name.startswith('refs/backup/'):
      continue
    prefix = 'refs/'
    if name.startswith('refs/remotes/origin/'):
        prefix = 'refs/remotes/origin/'
    if name.startswith(prefix + 'heads/'):
      if object_type not in ["commit"]:
        #TODO error
        pass
      if name in last_seen_refs and not git_is_ancestor(target, seen + name, object_hash):
        #TODO error
        g.archive(name, object_hash, target)
    elif name.startswith(prefix + 'tags/'):
      if object_type not in ["tag", "commit"]:
        #TODO error
        pass
      if name in last_seen_refs:
        #TODO error
        g.archive(name, object_hash, target)
    else:
      #TODO error
      if name in last_seen_refs:
        g.archive(name, object_hash, target)

    g.update(seen + name, object_hash)
    print('Discard: ' + seen + name)
    last_seen_refs.discard(name)

  if 0 != len(last_seen_refs):
    print('Remains: ')
    print(last_seen_refs)
    for name in last_seen_refs:
      g.archive(name, seen + name, target)
      g.delete(seen + name, seen + name)
  g.close()

def git_refs_filter(target, prefix):
  for ref in git_refs(target):
      prefix_len = len(prefix)
      _, _, name = ref.split()
      if name.startswith(prefix):
        yield name[prefix_len:]

def git_is_ancestor(directory, ancestor, inheritor):
  p = sh.git('-C', directory, 'merge-base', '--is-ancestor', ancestor, inheritor, _ok_code=[0,1])
  return 0 == p.exit_code

class GitRefUpdate:
  def __init__(self, directory):
    self.pipe_read, self.pipe_write = os.pipe()
    self.pipe = os.fdopen(self.pipe_write, 'w')
    self.process = sh.git("-C", directory, "update-ref", '--stdin', _bg=True, _in=self.pipe_read)
    self.first = True

  def update(self, name, value):
    self.pipe.write("update %s %s\n" % (name, value))

  def create(self, name, value):
    self.pipe.write("create %s %s\n" % (name, value))

  def archive(self, name, value, target):
    #TODO quote differently, less much?
    name = urllib.parse.quote(name, safe='')
    #TODO test git_commit_date with other object types, like tag
    date = git_commit_date(target, value)
    # no : in git refs allowed
    date = date.replace(':', '_')
    name = 'refs/backup/old-refs/' + name + '/' + date
    #TODO check if it exists and count up
    name = name + 'N0'
    print('Archive: ' + name + ' ' + value)
    if not self.first:
      raise Exception()
    self.first = False
    self.create(name, value)

  def delete(self, name, value):
    self.pipe.write("delete %s %s\n" % (name, value))

  def close(self):
    self.pipe.write('option no-deref\n')
    self.pipe.flush()
    self.pipe.close()
    print(self.process.wait())

def ensure_config(target):
  #TODO
  # we allow all ref updates because we need to be able to not miss them
  sh.git('config', 'remote.origin.fetch', '+refs/*:refs/remotes/origin/*')
  pass

def git_backup(source, target):
  if not os.path.lexists(target):
    sh.git("clone", "--bare", '--config', 'remote.origin.fetch=+refs/*:refs/remotes/origin/*', source, target, _fg=True)
    # --mirror sets mirror and fetch = +refs/*:refs/*
    os.chdir(target)
    ensure_config(target)
    print('\n'.join(git_refs(target)))
    g = GitRefUpdate(target)
    for ref in git_refs(target):
      object_hash, object_type, name = ref.split()
      g.delete(name, object_hash)
      g.create(re.sub(r'^refs/', r'refs/remotes/origin/', name), object_hash)
    g.close()
    print('\n'.join(git_refs(target)))
    sh.git("fetch", "--prune", "-v", _fg=True)
    backup_refs(target)
  else:
    os.chdir(target)
    ensure_config(target)
    backup_refs(target)
    sh.git("fetch", "--prune", "-v", _fg=True)
    backup_refs(target)

def git_refs(directory):
  return sh.git('-C', directory, 'for-each-ref', _iter="out")

def git_hash_head(directory):
  return str(sh.git('-C', directory, 'log', '-1', '--format=%H', '--no-show-signature', _tty_out=False)).strip()

def git_commit_date(directory, commit_hash='HEAD'):
  date = str(sh.git('-C', directory, 'log', '-1', '--format=%ci', '--no-show-signature', commit_hash, _tty_out=False)).strip()
  date = re.sub(r" ([^ ]+) ", r'T\g<1>Z', date)
  return date

@pytest.fixture
def tmpdir():
  with tempfile.TemporaryDirectory() as tmpdirname:
      yield tmpdirname

class GitBackup:
  def __init__(self, tmpdir):
    self.source = tmpdir + "/source-git"
    self.target = tmpdir + "/backup-git"
    self.count = 1
    self.add_expected_refs = set()

  def cd(self):
    os.chdir(self.source)

  def backup(self):
      git_backup(self.source, self.target)

  def commit(self):
      date = time.strftime('%Y-%m-%dT%H:%M:%SZ+0100', time.gmtime(self.count*60*60*24))
      new_env = os.environ.copy()
      new_env["GIT_COMMITTER_DATE"] = date
      print(sh.git("-C", self.source, "commit", "--allow-empty", "-m", "commit number " + str(self.count), _env=new_env))

  def add_expected_ref(self, commit_hash, name):
      self.add_expected_refs.add(commit_hash + ' commit\t' + name + '\n')

  def add_head(self):
      #TODO don't use was fixed by initial rename
      h = git_hash_head(self.source)
      # the master branch in the backup repo is created during initial clone
      self.add_expected_ref(h, 'refs/heads/master')
      self.add_expected_ref(h, 'refs/backup/last-seen-refs/refs/heads/master')

  def assert_refs(self):
      print(sh.cat(self.target + '/config'))
      refs = set(git_refs(self.source))
      print(refs)
      print('..source mod:')
      refs = { re.sub(r'\trefs/', r'\trefs/remotes/origin/', ref) for ref in refs }
      print(refs)
      print('..and add:')
      refs |= self.add_expected_refs
      print(refs)
      print('..target on right side:')
      print(set(git_refs(self.target)))
      assert  refs == set(git_refs(self.target))

@pytest.fixture
def git(tmpdir):
  git = GitBackup(tmpdir)
  os.mkdir(git.source)
  os.chdir(git.source)
  sh.git("init")
  sh.git("config", "user.name", "git-backup-test-name")
  sh.git("config", "user.email", "git-backup@example.com")
  git.commit()
  yield git

def test_run_once(git):
  git.add_expected_ref(git_hash_head(git.source), 'refs/backup/last-seen-refs/refs/remotes/origin/heads/master')
  git.backup()
  git.assert_refs()

def test_run_twice(git):
  git.backup()
  git.commit()
  git.add_expected_ref(git_hash_head(git.source), 'refs/backup/last-seen-refs/refs/remotes/origin/heads/master')
  git.backup()
  git.assert_refs()

def test_added_branch(git):
  git.add_expected_ref(git_hash_head(git.source), 'refs/backup/last-seen-refs/refs/remotes/origin/heads/master')
  git.cd()
  sh.git("branch", "test/branch")
  git.add_expected_ref(git_hash_head(git.source), 'refs/backup/last-seen-refs/refs/remotes/origin/heads/test/branch')
  git.backup()
  git.assert_refs()

def test_deleted_branch(git):
  git.add_expected_ref(git_hash_head(git.source), 'refs/backup/last-seen-refs/refs/remotes/origin/heads/master')
  git.cd()
  sh.git("branch", "test/branch")
  git.backup()
  git.cd()
  sh.git("branch", "-d", "test/branch")
  git.backup()
  date = git_commit_date(git.source)
  date = date.replace(':', '_')
  git.add_expected_ref(git_hash_head(git.source), 'refs/backup/old-refs/refs%2Fremotes%2Forigin%2Fheads%2Ftest%2Fbranch/' + date + 'N0')
  git.assert_refs()

def test_git_clone_refspec(git):
  sh.git("clone", "--bare", '--config', 'remote.origin.fetch=+refs/*:refs/remotes/origin/*', git.source, git.target, _fg=True)
  print('The fetch refspec config passed during clone has no effect.')
  assert set(git_refs(git.source)) == set(git_refs(git.target))

def test_git_clone_refspec_rename(git):
  sh.git("clone", "--bare", '--config', 'remote.origin.fetch=+refs/*:refs/remotes/origin/*', git.source, git.target, _fg=True)
  print('To work around the ignored fetch refspec config during clone apply the rename afterwards.')
  g = GitRefUpdate(git.target)
  for ref in git_refs(git.target):
    object_hash, object_type, name = ref.split()
    g.delete(name, object_hash)
    g.create(re.sub(r'^refs/', r'refs/remotes/origin/', name), object_hash)
  g.close()
  git.assert_refs()

def test_git_clone_refspec_fetch(git):
  sh.git("clone", "--bare", '--config', 'remote.origin.fetch=+refs/*:refs/remotes/origin/*', git.source, git.target, _fg=True)
  g = GitRefUpdate(git.target)
  for ref in git_refs(git.target):
    object_hash, object_type, name = ref.split()
    g.delete(name, object_hash)
    g.create(re.sub(r'^refs/', r'refs/remotes/origin/', name), object_hash)
  g.close()
  print('The fetch refspec config passed during clone takes effect on a subsequent fetch.')
  sh.git("fetch", "--prune", "-v", _fg=True)
  git.assert_refs()

