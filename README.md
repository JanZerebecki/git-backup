# git-backup

Like git fetch, but without loosing history.

Status: Proof of concept. Do not rely on this yet.

By default when a branch/tag in a git remote is overwritten (e.g.  `git push
-f`), git fetch and pull will only tell you by changing a little detail in
their output, overwrite it locally and delete the previous content after about
4 month. This is undesirable for backups, protected branches and reproducible
builds.

```console?comments=true
# create a git repo with two branches
$ mkdir repo-source && cd repo-source && git init
repo-source $ git commit --allow-empty -m 'first commit'
repo-source $ git branch test/branch
repo-source $ git for-each-ref
0000000000000000000000000000000000000001 commit	refs/heads/master
0000000000000000000000000000000000000001 commit	refs/heads/test/branch
# make a backup
repo-source $ git backup . ../repo-backup
# modify master and delete the other branch
repo-source $ git commit --amend --allow-empty -m 'first commit, amended'
repo-source $ git banch -d test/branch
repo-source $ git for-each-ref
0000000000000000000000000000000000000002 commit	refs/heads/master
# run backup again
repo-source $ git backup . ../repo-backup
repo-source $ cd repo-backup
# deleted / overwritten stuff is still there under refs/backup/old-refs
repo-backup $ git for-each-ref
0000000000000000000000000000000000000002 commit	refs/heads/master
0000000000000000000000000000000000000002 commit	refs/backup/last-seen-refs/refs/remotes/origin/heads/master
0000000000000000000000000000000000000001 commit	refs/backup/old-refs/refs%2Fremotes%2Forigin%2Fheads%2Fmaster/1970-01-02T00_00_00Z+0100N0
0000000000000000000000000000000000000001 commit	refs/backup/old-refs/refs%2Fremotes%2Forigin%2Fheads%2Ftest%2Fbranch/1970-01-02T00_00_00Z+0100N0
```

Planned features:

* Make it ready for general use
* Different exit code for when the source overwrote something instead of 0 and
  also a different exit code for when fetching failed (e.g. due to network
  error).
* Ensure all submodules are also backed up.

